import { GetUsers, AddUser, DeleteUser, UpdateUser } from './../store/app.action';
import { Appstate } from './../store/app.state';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    userForm: FormGroup | any;
    userInfo: [] | any;
    @Select(Appstate.selectStateData) userInfo$: Observable<any> | undefined
    constructor(private store: Store, private fb: FormBuilder) { }

    ngOnInit(): void {
        this.userForm = this.fb.group({
            id: [''],
            name: [''],
            username: [''],
            email: [''],
            phone: [''],
            website: ['']
        })
        this.store.dispatch(new GetUsers);
        this.userInfo$?.subscribe((returnData) => {
            this.userInfo = returnData;
        })
    }

    addUser() {
        this.store.dispatch(new AddUser(this.userForm.value));
        this.userForm.reset();
    }

    deleteUser(i: number) {
        this.store.dispatch(new DeleteUser(i));
    }

    updateUser(i: number, id: number, name: string, username: string, email: string, phone: string, website: string) {
        const newData = {
            id: id,
            name: name,
            username: username,
            email: email,
            phone: phone,
            website: website
        }
        this.store.dispatch(new UpdateUser(newData, id, i));
    }
}